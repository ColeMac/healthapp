//
//  Section.swift
//  HealthApp
//
//  Created by Moisés Córdova on 12/25/18.
//  Copyright © 2018 Moisés Córdova. All rights reserved.
//

import Foundation

struct Section {
    var type: String!
    var data: [String]!
    var expanded: Bool!
    
    init(type: String, data: [String], expanded: Bool) {
        self.type = type
        self.data = data
        self.expanded = expanded
    }
}
