//
//  DoctorTableViewCell.swift
//  HealthApp
//
//  Created by Moisés Córdova on 12/22/18.
//  Copyright © 2018 Moisés Córdova. All rights reserved.
//

import UIKit

class DoctorCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var directionLabel: UILabel!
}
