//
//  DatePickerCell.swift
//  HealthApp
//
//  Created by Moisés Córdova on 1/3/19.
//  Copyright © 2019 Moisés Córdova. All rights reserved.
//

import UIKit

class DatePickerCell: UITableViewCell {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let actualDate = Date()
        datePicker.minimumDate = actualDate
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 1, to: actualDate)
    }
}
