//
//  UserActivityType.swift
//  HealthApp
//
//  Created by Moisés Córdova on 12/27/18.
//  Copyright © 2018 Moisés Córdova. All rights reserved.
//

import Foundation

struct UserActivityType {
    static let sendToServer = "com.moisesCordova.HealthApp.SendToServer"
    static let showMyId = "com.moisesCordova.HealthApp.ShowMyId"
    static let scanId = "com.moisesCordova.HealthApp.ScanId"
}
